console.log("test");

// ---------------------------
// Functii de Calback

let saveServerResponse = function(data) {
    //localStorage.setItem("serverData", "{id: '1', user: 'Mike'}");
    localStorage.setItem("serverData", data);
    console.log(localStorage.getItem("serverData"));
}
function requestServer(func) {
    let user = {
        id: "1",
        userName: "Mike"
    }
    if (user && user.id) {
        return func(user);
    }
    return alert("request error");
}
requestServer(saveServerResponse);

// ---------------------------
// Functii Recursive

// function a(x) {
//     console.log(x);

//     // condition to prevent endless loop
//     if (x > 0) {
//         a(x - 1);
//     }
// }
// a(5);

// ---------------------------
// Functii Recursive

// function rotateWord(word, initialWord) {
//     console.log(word);
//     let letter = word[word.length - 1];
//     word = word.slice(0, word.length - 1);
//     word = letter + word;
//     if (word !== initialWord) {
//         rotateWord(word, initialWord);
//     }
// }
// rotateWord("cuvant", "cuvant");

// function rotateWord(word, initialWord) {
//     console.log(word);
//     //let str = "cuvant";
//     let letter = word.slice(-1);
//     word = word.slice(0, word.length - 1);
//     word = letter + word;
//     if (word !== initialWord) {
//         rotateWord(word, initialWord);
//     }
// }
// rotateWord("cuvant", "cuvant");



// ---------------------------
// Functii Iterative

// function rotateWord(word) {
//     let initialWord = word;
//     do {
//         console.log(word);
//         let letter = word[word.length - 1];
//         word = word.slice(0, word.length - 1);
//         word = letter + word;
//     } while (word !== initialWord);
// }
// rotateWord("carte");

// ---------------------------
// Obiecte

let student = {
    name: "Vlad",
    surname: "Nicolae",
    age: 30,
    courses: [
        {
            name: "Economy",
            hours: 25,
            description: "Course"
        },
        {
            name: "Managemnt",
            hours: 20,
            description: "Course"
        }
    ]
}

// ----

console.log(student);
let courseConstructor = function(name, hours, description) {
    return {
        name: name,
        hours: hours,
        description: description
    }
}
console.log(courseConstructor("Mathematics", 35,  "Course"));
student.courses.push(courseConstructor("Mathematics", 35,  "Course"));

let studentContructor = function(name, surname, age, courses) {
    return {name, surname, age, courses};
}
console.log(studentContructor("Alex", "Preston", 25, []));

function studentCopy(student) {
    return{...student};
}
let newStudent = studentCopy(student);
console.log(newStudent);

newStudent.name = "Viorel";
newStudent.courses[0] = "Computer Science";
console.log(student);
console.log(newStudent);

// ----

// let stringifiedStudent = JSON.stringify(student);
// console.log(stringifiedStudent);
// let newStudent = JSON.parse(stringifiedStudent);
// newStudent.courses[0] = {
//     name: "Geography",
//     hours: 25,
//     description: "Course"
// };
// console.log(student);
// console.log(newStudent);

// ---------------------------
// Cuvinte Anagrame
// silent, listen -> sunt cuvinte anagrame

// function anagram(word1, word2){
//     if (word1.lenght !== word2.lenght) {
//         return false;
//     }
//     let word1freq = [];
//     let word2freq = [];

//     for(let i = 0; i < word1.lenght; i++) {
//         if(word1freq[word1.charCodeAt(i) - 97]) {
//            word1freq[word1.charCodeAt(i) - 97]++;
//         }
//         else {
//             word1freq[word1.charCodeAt(i) - 97] = 1;
//         }
//     }
//     for (let i = 0; i < word2.lenght; i++) {
//         if(word2freq[word2.charCodeAt(i) - 97]) {
//             word2freq[word2.charCodeAt(i) - 97]++;
//         }
//         else {
//             word2freq[word2.charCodeAt(i) - 97] = 1;
//         }
//     }

//     console.log(word1freq);
//     console.log(word2freq);
//     for(let i = 0; i < word1freq.lenght; i++) {
//         if(word1freq[i] !== word2freq[i]) {
//             return false;
//         }
//     }
//     return true ;
// }
// console.log (anagram("silent", "listen"));

// ---------------------------
// Fibonaci

// function fibonacciInteractiv (element) {
//     if (element <= 1) {
//         return 1;
//     }
//     else {   
//         let array = [1,1,];
//         for (let i = 2; i < element; i++) {
//             array.push(array[i-2]+ array[i-1]);
//         }
//     }
//     return array[element];
// } 

// function fibonacciInteractiv(element) {
//     if (element <= 1) {
//         return 1;
//     }
//     else {
//         let a = 1;
//         let b = 1;
//         let c;
//         for (let i = 2; i <= element; i++){
//             c = a + b;
//             a = b;
//             b = c;
//         }
//         return c;
//     }
// }
// console.log(fibonacciInteractiv(10));

// function fibonacciRecursiv (number){
//     if (numer <= 1 ){
//         return 1;
//     }
//     else{
//         return fibonacciRecursiv
//     }
// }

// function validateInput (userInput){
//     if (userInput === null){
//         return "Put a valid input";
//     }
//     if (userInput === ""){

//     }
// }

var num=123, str = "foo";
function  f(num, str) {
    num += 1;
    str += "bar";
    console.log("inside of f:", num, str);

}
f (num,str);
console.log('outside of f:' , num, str);