// un sir de numere creat din cele din inaintea lui
// 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89...
// 0  1  2  3  4  5   6   7   8   9  10
// 3+5=8
// element[i]=element[i-2]+element[i-1]
//     c          a            b
/*
function fibonacciInteractiv (element) {
    if (element <= 1) {
        return 1;
    }
    else {
        let array = [1, 1];
        for (let i=2; i<=element; i++){
            array.push(array [i-2]+array[i-1]);
        }
        return array[element];
    }
}

console.log (fibonacciInteractiv(6));
*/

/*
function fibonacciInteractiv (element) {
    if (element <=1) {
        return 1;
    }
    else{
        let a=1;
        let b=1;
        let c;
        for (let i=2; i<=element; i++) {
            c=a+b;
            a=b;
            b=c;
        }
        return c;
    }
}

console.log (fibonacciInteractiv(10));
*/

/*
//elementul de pe pozitia 6 il consideram fibonacciRecursiv de 6 = fibonacciRecursiv de 5 + fibonacciRecursiv de 4
//elementul de pe pozitia 5 il consideram fibonacciRecursiv de 5
//elementul de pe pozitia 4 il consideram fibonacciRecursiv de 4
function fibonacciRecursiv(number) {
    if (number <=1) {
        return 1;
    }  
    else {
        return fibonacciRecursiv(number-1)+fibonacciRecursiv(number-2)
    }
}

console.log (fibonacciRecursiv(7))
*/

/*
function validateInput (userInput) {
    console.log(userInput);
    if (userInput === null) {
        return "Please put a valid input!";
    }

    if (userInput === "") {
        return "Please add at least one character!";
    }

    if (userInput === undefined) {
        return "Your input is undefined :("
    }

    return "Your input is valid";
}

console.log(validateInput(null));
console.log(validateInput(""));
console.log(validateInput("abc"));
console.log(validateInput());
*/