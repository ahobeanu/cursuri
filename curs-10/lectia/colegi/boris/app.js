/*
function a(x) {
    console.log(x);

    //conditie care sa previna un loop infinit
    if (x>0){
        a(x-1); //functia se acceseaza pe ea
    }
}

a(5); //5 4 3 2 1
*/

/*
function rotateWord(word, initialWord) { // facem o functie cu 2 parametrii: cuvant nou si cuvant initial
    console.log(word); //arata-mi cuvantul nou
        let letter=word[word.length-1] //creaza un nou item numit litera care se identifica ca fiind litera de la sfarsitul cuvantului 
        word=word.slice(0, word.length-1) //taie cuvantul si pastreaza de la 0 la penultima litera
        word=letter+word //adauga litera la inceput
    if (word!==initialWord) { // atata timp cat cuvantul nou este diferit de cuvatuntul initial
        rotateWord (word, initialWord) // fooseste functia rotateWord
    }
}
function rotateWordWrapper(word) //cream o functie care sa imbrace functia care roteste dar care are un singur parametru de la user
    rotateWord (word, word) // functia are 2 parametrii cu acelasi cuvant 
    
}
rotateWordWrapper ("cuvant test")
*/

/*
function rotateWordIterative(initialWord) { //cream o functie de rotire iterrativa
    
    let word = initialWord //cream un cuvant nou care e identic cu cel initial
    console.log(word); // arata cuvantul nou

    //taiem o data ca sa facem cuvantul nou diferit de cuvantul initial
    let letter=word[word.length-1] //identifica litera ca fiind ultima litera din cuvantul nou
    word=word.slice(0, word.length-1) //taie cuvantul cu exceptia ultimei litere
    word=letter+word // adauga litera la inceputul noului cuvant

    while (word !== initialWord) { //pornim while cu aceeasi chestii de mai sus
        console.log(word);
        let letter=word[word.length-1]
        word=word.slice(0, word.length-1)
        word=letter+word
    }
    console.log(word);
}

rotateWordIterative("cuvant test nou")
*/

/*
// Exercitiu nou:
// Create a JavaScript object, representing a student.
// Student has: name, surname, age.
// Student has also an array of courses.
// Each course has a name, number of hours and a short description.
// Write a function that will create a sample Course object and fill it with some data.
// Write a function that will create a sample Student object and fill it with some data.
// Write another function that will copy the previously created Student and make a new one.
// Check if the students is properly copied - including his courses (change in copied student's courses should not affect the original ones)!


let student = { //cream un obiect cu nume, nume de familie, varsta si alte obiecte (cursurile) care au la randul lor nume, nr ore si descriere
    name: "Diana",
    surname: "Dascalescu",
    age: 25,
    courses:[
        {
            name: "Management", 
            hours: 20,
            description: "curs complicat",
        },
        {
            name: "Economie", 
            hours: 13,
            description: "curs complicat",
        }
    ]
}
console.log(student);

// Write a function that will create a sample Course object and fill it with some data.
let courseConstructor = function(name, hours, description) {
    return {name, hours, description}
}

console.log(courseConstructor("Informatica", 30, "Dificil"));

// Write a function that will create a sample Student object and fill it with some data.
student.courses.push(courseConstructor("Informatica", 30, "Dificil"));
console.log(student)

let studentConstruct = function(name, surname, age, courses){
    return{name, surname, age, courses};
}
console.log(studentConstruct("Diana", "Maria", 23, [] ));


// Write another function that will copy the previously created Student and make a new one.
function studentCopy(student){
    return {...student};
}

let newStudent = studentCopy(student);
console.log(newStudent);

newStudent.name = "Viorel";
newStudent.courses[0] = "Geografie"; //problema: cand modificam un array copiat se modifica si in original, spre deosebire de objects

console.log(student, newStudent);
*/

