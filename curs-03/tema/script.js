// Create an array, containing 10 numbers.
var numere = [1, 8, 9, 3, 8, 12, 23, 19, 12, 19];
console.log(numere);

// Remove the last element from the array and write it on the screen.
numere.pop();
console.log(numere);

// Add a new element to the end of the array.
numere.push(34);
console.log(numere);

// Change the value of the array's second element to zero.
numere[1] = 0;
console.log(numere);


//  Hint! 
//  Veti avea nevoie de functiile pop() si push() asociate unui array