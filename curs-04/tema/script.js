// var a = 5;
// var b = 6;
// var c = 9;
// function ade(x, y, z) {
//     var suma= x + y + z;
//     return suma;
// }
// var sum = ade(5, 6, 9);
// //console.log(sum);

// function cry(x, y) {
//     var pr = x * y;
//     return pr;
// }
// var carte = cry(5, 6);
// //console.log(carte);

// for (var i = 10; i < 21; i++) {
//     if (i%2!=0){
//         console.log(i + ' ');  
//     }  
// }
// var i =10
// while (i<21) {
//     console.log (i+ ' ');
//     i++
// }
// /*
// function afisare (x, y) {
//     for (var i = x; i < y; i++) {
//         console.log(i + ' ');
//     }
// }
// afisare(10,20);
// */

// /*
// function afisare (x, y) {
//     var x = i;
//     while(i < y) {
//         console.log(i + ' ');
//         i++;
//     }
// }
// */

// var a = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
// a.pop();
// a.push(32);
// a[1] =0;

// for (var i = 0; i < a.length; i++) {
//     if ((i + 1) % 3 == 0) {
//         a[i] = 1;
//     }
// }

function afisare(x) {
    for (var i = 0; i < x.length; i++) {
        console.log(x[i]);    
    }
    var i = 0;
    while (i < x.length) {
        console.log(x[i]);
        i++;
    }
}
// afisare(a);

// console.log(a);
// var b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function putere (a, b) {
    var rezultat = 1;
    for (var i = 1; i <= b; i++) {
        rezultat = rezultat * a;
    }
    return rezultat;
}
var rez = putere(2, 5);
console.log(rez);
// console.log(putere(2, 5));

document.getElementById("caluleaza").onclick = function() {
    var baza = parseInt(document.getElementById("baza").value);
    var exponent = parseInt(document.getElementById("exponent").value);
    var rezultat = putere(baza, exponent);
    document.getElementById("rezultat").value = rezultat;
}

// function yearsUntilRetirement(birthYear, firstName){
//     const age = 2020 - birthYear;
//     const retirement = 65 - age;
//    // return retirement;
//     return `${firstName} retires in ${retirement} years`;
// }
// console.log(yearsUntilRetirement(1986, "Ade"));

//Functions Calling other functionsa:
function calcAge(bYear) {
    return 2020-bYear;
}

var retirementAge = 67;
function yearsUntilRetirement(birthYear, firstName) {
    var age = calcAge(birthYear);
    var retirement = retirementAge - age;

    if (retirement > 0) {
        console.log(`${firstName} retires in ${retirement} years`);
        //return retirement;
    } else {
        console.log(`${firstName} has already retired`);
        //return retirement;  
    }
}
yearsUntilRetirement(1989, "Flory");
yearsUntilRetirement(1979, "Raul");

function percentageOFWorld (population) {
    const worldPopulation = 7900 * 1.0e6;
    var percentage = (population * 100) / worldPopulation;
    return (percentage + ' Procentul Chinei');
} 
console.log(percentageOFWorld(1441 * 1.0e6));

function ecuatiaDeGradul (a, b, c) {
    var delta = (b * b) - (4 * a * c);
    if (delta < 0) {
        console.log("Ecuatia nu are solutii reale");
        return;
    }
    if (delta == 0) {
        var x = -b / 2 * a;
        console.log('Ecuatia are o singura radacina x = ' + x);
        return;
    }
    if (delta > 0){
        var x1 = (-b + Math.sqrt(delta)) / (2 * a);
        var x2 = (-b - Math.sqrt(delta)) / (2 * a);
        console.log('Ecuatia are doua radacini x1 = ' + x1 + " si x2 = " + x2);
        return;
    }
}
ecuatiaDeGradul(8, 15, 6);