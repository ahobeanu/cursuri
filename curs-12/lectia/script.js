// let name = "John";
// let surname = "Popescu";
//  let age=25; 
// if (age && age>0) {
//     console.log(`My name is ${name} ${surname}.
//     My ages is ${age}`);
// }
// else {
//     console.log(`My name is ${name} ${surname}.
//     My ages is 0`);
// }
// console.log(`My name is ${name} ${surname}.
//  My ages is ${age && age>0? age:0}`);

//  Distruction

// function animal(name="Urs", size="Mare") {
//     this.name = name;
//     this.size = size;
//     // this.hello = function() {} {console.log(this.name)}
// }
// let lup = new animal("Lup", "Mediu");
// let rata = new animal("rata", "Mic");

// animal.prototype.hello = function(){
//     console.log(this.name);
// }
// // lup.hello = function () {
// //     console.log(this.name);
// // }

// console.log(lup);
// lup.hello();
// console.log(rata);
// rata.hello();

// Class aniaml ()

class Animal {
    constructor(name, size) {
        this.name = name;
        this.size = size;
    }
    hello = function name() {
        console.log(this.name);
    }
}
let lup = new Animal("lup", "mare");
console.log(lup);

class Bird extends Animal {
    _speed =200;
    static numberOfLegs = 2;
    constructor(name, size, speed) {
        super(name, size);
        this._speed = speed;
        this.getSpeed = function() {
            return _speed;
        }
    }
    get tellspeed () {
        console.log(this.name, this._speed);
    }
}

let cioara = new Bird("cioara", "mica", 300);
console.log(cioara);
cioara.hello();
cioara.tellspeed;
console.log(Bird.numberOfLegs);
console.log(cioara instanceof Bird);
console.log(cioara instanceof Animal);
console.log(cioara instanceof Object);
console.log(cioara instanceof String);
console.log(cioara instanceof Array);


