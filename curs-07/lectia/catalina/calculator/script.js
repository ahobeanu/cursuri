var result = 0;

var currentNumber = "0";

var lastNumber = null;

var operatorRequested = false;

var lastOperatorRequested = "";

var display = document.getElementById("value");

var lastTimeOperatorPressed = false;

refreshDisplay(currentNumber);

function refreshDisplay(content) {
    display.innerHTML = content;
}

function keyPressed(key) {
    if(currentNumber === "0" || operatorRequested === true) {
        currentNumber = "";
        operatorRequested = false;
    }

    currentNumber = currentNumber.concat(key);
    refreshDisplay(currentNumber);

    lastTimeOperatorPressed = false;
}

function reset() {
    currentNumber = "0";
    result = 0;
    refreshDisplay(currentNumber);
    lastNumber = null;
}

function plus() {
    if(lastTimeOperatorPressed === false) {
        result = result + parseFloat(currentNumber);
        refreshDisplay(result);
        operatorRequested = true;
        lastOperatorRequested = "+";
        lastTimeOperatorPressed = true;
    }
}

function  minus() {
    if(lastTimeOperatorPressed === false) {
        if(lastNumber === null) {
            result = parseFloat(currentNumber);    
        } else {
            result =  parseFloat(lastNumber) - parseFloat(currentNumber);
        }

    // lastNumber = currentNumber;

        lastNumber = result;

        refreshDisplay(result);

        operatorRequested = true;
        lastOperatorRequested = "-";
        lastTimeOperatorPressed = true;
    }
}

function multiply() {
    if(lastTimeOperatorPressed === false) {
        if(lastNumber === null) {
            result = parseFloat(currentNumber);
        } else {
            result = parseFloat(lastNumber) * parseFloat(currentNumber);
        }

        lastNumber = result;

        refreshDisplay(result);
        operatorRequested = true;
        lastOperatorRequested = "*";
    }
}

function divide() {
    if(lastTimeOperatorPressed === false) { 
        if(lastNumber === null) {
            result = parseFloat(currentNumber);
        } else {
            result = parseFloat(lastNumber) / parseFloat(currentNumber);
        }

        lastNumber = result;

        refreshDisplay(result);
        operatorRequested = true;
        lastOperatorRequested = "/";
    }
}

function equal() {
    switch (lastOperatorRequested) {
        case "+":
            plus();
            break;

        case "-":
            minus();
            break;

        case "*":
            multiply();
            break;

        case "/":
            divide();
            break;
    }
    lastOperatorRequested = "";
    lastNumber = null;
     currentNumber = result;
}





